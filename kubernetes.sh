export AWS_PROFILE=codefornrv
export KOPS_STATE_STORE=s3://codefornrv-kops-state-store
export ZONES="us-east-1a,us-east-1b,us-east-1c"
export MASTER_ZONES="us-east-1c"

kops create cluster \
--name k8s.aws.codefornrv.org \
--cloud=aws \
--zones $ZONES \
--master-zones $MASTER_ZONES \
--master-size t2.micro \
--node-size t2.micro \
--node-count 2 \
--ssh-public-key ~/.ssh/codefornrv-k8s.pub \
--kubernetes-version 1.9.6


kubectl apply -f kubernetes/helm/tiller-rbac.yml
helm init --service-account tiller

helm install --name certs stable/cert-manager

helm install stable/nginx-ingress --name nginx -f kubernetes/helm/nginx-config.yml

kubectl apply -f kubernetes/cert-manager/cluster-issuer.yml

# Add the parks API (might not process in right order)
kubectl apply -f kubernetes/parks/